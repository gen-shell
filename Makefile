all:
	${CC} -o gen-shell src/*.cpp ${CFLAGS} -lstdc++ -lreadline ${LDFLAGS}

install: all
	cp gen-shell /usr/local/bin/gen-shell

uninstall:
	rm /usr/local/bin/gen-shell

.PHONY: all
