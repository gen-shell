// gen-shell - the generic REPL
// Copyright (c) 2021, Armaan Bhojwani <me@armaanb.net>

#include <iostream>
#include "sarge.h"

#include <vector>
#include <string>

#include <readline/readline.h>
#include <readline/history.h>

////////////////////////////////////////////////////////////////////////////////

using namespace std;

const std::string
getResponse(const std::string & prompt) {
  std::string response {
    ""
  };

  // Display prompt, get input
  char * line_read = readline(prompt.c_str());
  if (!line_read) {
    std::cout << "\n";
    response = "<EOF>";
  } else {
    if (*line_read) {
      add_history(line_read);
    }

    response = std::string(line_read);
    free(line_read);
  }

  return response;
}

////////////////////////////////////////////////////////////////////////////////

int
main(int argc, char** argv)
{
  // Command line arguments
  Sarge sarge;

  sarge.setArgument("c", "command", "Command to convert to REPL", true);
  sarge.setArgument("h", "help", "Show this message.", false);
  sarge.setArgument("p", "prompt", "Define a custom prompt", true);
  sarge.setArgument("q", "quotes", "Treat whole input as argv[1]", false);
  sarge.setDescription("Make a REPL from any executable");
  sarge.setUsage("gen-shell <options>");

  if (!sarge.parseArguments(argc, argv)) {
    std::cerr << "Could not parse command line arguments" << std::endl;
    return 1;
  }

  if (sarge.exists("help")) {
    sarge.printHelp();
    return 0;
  }

  string arg_cmd;
  sarge.getFlag("command", arg_cmd);

  string prompt = "";
  sarge.getFlag("prompt", prompt);
  if (prompt == "") {
    prompt = "% ";
  }

  // Do the stuffs!
  while (true) {
    auto command = getResponse(prompt);

    if (command == "<EOF>" || command == "exit" || command == "quit" ) {
      return 0;
    } else {
      string whole_command = arg_cmd + " ";
      if (sarge.exists("quotes")) {
        whole_command = whole_command + "\"" + command + "\"";
      } else {
        whole_command += command;
      }
      system(whole_command.c_str());
    }
  }
}

////////////////////////////////////////////////////////////////////////////////
